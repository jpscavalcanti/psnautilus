class del1:
    def problem1(n1, nx, rz):
        temp = n1
        temp_list = [n1]
        if rz%2==0:
            while(True):
                temp += rz
                if (temp>nx):
                    break
                temp_list += [temp]
            return temp_list
        else:
            while(True):
                temp *= rz
                if (temp>nx):
                    break
                temp_list += [temp]
            return temp_list
    
    def problem2(i):
        count = 0
        while(i!=0):
            r = i%2
            count+=r
            i = int((i-r)/2)
        return count
    
    def problem3(number, sum, data):
        tipos_dinheiro = [1, 2, 5, 10, 20, 50, 100, 200]
        count = 0
        for i in tipos_dinheiro[::-1]:
            
            if(sum + i == number):
                count+=1

            elif(sum + i > number):
                count+=0

            else:
                if(not (str(sum+i) in data.keys())):
                    data[str(sum+i)] = del1.problem3(number, sum+i, data)
                count += data[str(sum+i)]

        return count
    
    def problem4(nf):
        temp=0
        for i in range(1, nf+1):
            temp += (i**i)%(10**10)
        return temp%(10**10)

if __name__ == "__main__":
    print("Problema 1:", del1.problem1(2, 13, 4))
    print("Problema 2:", del1.problem2(1234))
    print("Problema 3:", del1.problem3(200, 0, {}))
    print("Problema 4:", del1.problem4(1000))
