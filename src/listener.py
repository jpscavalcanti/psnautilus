#!/usr/bin/env python3

import rospy
from geometry_msgs.msg import Point
# from std_msgs.msg import Float32

class unit_vector():
    def __init__(self):
        rospy.init_node('listener', anonymous=True)
        self.pub = rospy.Publisher('unit_vector', Point, queue_size=10)
        self.sub = rospy.Subscriber('vector', Point, self.callback)

        rospy.spin()
    
    def callback(self, msg):
        x = msg.x
        y = msg.y
        z = msg.z
        lenght = (x**2 + y**2 + z**2)**.5
        unit = Point()
        unit.x = x/lenght
        unit.y = y/lenght
        unit.z = z/lenght

        self.pub.publish(unit)

if __name__ == '__main__':
    try:
        u = unit_vector()
    
    except rospy.ROSInterruptException:
        pass