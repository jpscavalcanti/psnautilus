#!/usr/bin/env python3

import rospy
from random import randint
from geometry_msgs.msg import Point

class SensorPosition():
    def __init__(self):
        rospy.init_node('talker', anonymous=True)

        self.pub = rospy.Publisher('vector', Point, queue_size=10)
    
    def start(self):
        rate = rospy.Rate(10)

        while not rospy.is_shutdown():
            v = Point()
            v.x = randint(-10,10)
            v.y = randint(-10,10)
            v.z = randint(-10,10)
            self.pub.publish(v)
            rate.sleep()


if __name__ == '__main__':
    try:
        s = SensorPosition()
        s.start()
    except rospy.ROSInterruptException:
        pass