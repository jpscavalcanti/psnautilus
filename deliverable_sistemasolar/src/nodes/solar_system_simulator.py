#!/usr/bin/env python3  
import rospy

import tf2_ros
import geometry_msgs.msg
import math


def handle_planets(name, radius, frequency):
    br = tf2_ros.TransformBroadcaster()
    t = geometry_msgs.msg.TransformStamped()

    # (name, radius, frequency) = rospy.get_param('/planets')[int(rospy.Time.now().to_sec())%8]

    t.header.stamp = rospy.Time.now()
    t.header.frame_id = "sun"
    t.child_frame_id = name
    t.transform.translation.x = radius*math.cos(rospy.Time.now().to_sec()*frequency*2*math.pi)
    t.transform.translation.y = radius*math.sin(rospy.Time.now().to_sec()*frequency*2*math.pi)

    t.transform.translation.z = 0.0

    t.transform.rotation.x = 0
    t.transform.rotation.y = 0
    t.transform.rotation.z = 0
    t.transform.rotation.w = 1

    br.sendTransform(t)

if __name__ == '__main__':
    rospy.init_node('solar_system')
    rate = rospy.Rate(10)

    # planet_names = ['Mercury', 'Venus', 'Earth', 'Mars', 'Jupiter', 'Saturn', 'Uranus', 'Neptune']
    # planets = []
    # for i in range(len(planet_names)):
    #     planets += [(planet_names[i], 2*i+1, (10-i)/30)]
    
    # rospy.set_param('/planets', planets)

    while not rospy.is_shutdown():

        for i in range(8):
            (name, radius, frequency) = rospy.get_param('/planets')[i]
            handle_planets(name, radius, frequency)

        rate.sleep()