#psnautilus

DELIVERABLE1:
    problema 1:
        A função guarda em temp o ultimo termo da sequencia atual e vai guardando estes em temp_list que é a lista que será retornada. Para saber qual tipo de sequência deve ser é feito o resto de rz.
    problema 2:
        É aplicado o algorítmo de mudança de base para a base dois e são contabilizados quando este é 1
    problema 3:
        Para resolver este problema a função percorre recursivamente todos os caminhos possíveis para combinações das moedas e contabiliza os válidos. Para poupar tempo é passada um dicionário onde é recordado o numero de caminhos possíveis para cada soma parcial
    problema 4:
        Para resolver este problema é feita a adição dos 10 primeiros dígitos de cada i**i até i=1000 e por fim são retornados os 10 primeiros dígitos dessa soma.