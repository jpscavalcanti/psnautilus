class Jogador:
    def __init__(self, nome, pontos, blocks, assists, steals, posicao):
        self.nome = nome
        self.pontos = pontos
        self.blocks = blocks
        self.assists = assists
        self.steals = steals
        self.posicao = posicao
    
    def __add__(self, other):
        return self.pontos + other.pontos
    
    def __sub__(self, other):
        return self.pontos - other.pontos

class Time:
    def __init__(self, nome):
        self.nome = nome
        self.jogadores = []

    def addJogador(self, jogador):
        if(len(self.jogadores)>=5):
            print("ERRO: Time já está cheio")
        else:
            self.jogadores += [jogador]
    
    def totalPontos(self):
        soma = 0
        for jogador in self.jogadores:
            soma += jogador.pontos
        return soma

    def __repr__(self):
        if len(self.jogadores)!=0:
            s = self.nome+":\n"
            for i in self.jogadores:
                s+= i.nome+" - "
                s+="posicao: "+i.posicao+" - "
                s+="pontos: "+str(i.pontos)+" - "
                s+="blocks: "+str(i.blocks)+" - "
                s+="assists: "+str(i.assists)+" - "
                s+="steals: "+str(i.steals)+"\n"
        else:
            s = self.nome
        return s
    
    def __add__(self, other):
        return self.totalPontos() + other.totalPontos()
    
    def __sub__(self, other):
        return self.totalPontos() - other.totalPontos()
