import numpy as np

class Trecho:
    def __init__(self, label, valores, taxa):
        if not(taxa in range(1,11)):
            print("ERRO: taxa fora do intervalo")
            return None
        else:
            self.label = label
            self.valores = valores
            self.taxa = taxa
            temp = 0
            for i in valores:
                temp += i
            temp /= len(valores)
            self.media = temp
    
    def __repr__(self):
        s = ""
        s += "label: "+self.label+"\n"
        s += "taxa: "+str(self.taxa)+" Hz\n"
        s += "média: "+str(self.media)+"\n"
        s += "valores\n"
        for i in range(len(self.valores)):
            if self.valores[i]<0:
                s+="{:.2f}: {:.5f} ".format(i/self.taxa, self.valores[i]) + "\n"
            else:
                s+="{:.2f}:  {:.5f} ".format(i/self.taxa, self.valores[i]) + "\n"
        return s

    def __mul__(self, other):
        if(self.taxa!=other.taxa):
            print("ERRO: sinais com taxas diferentes")
            return None
        else:
            return Trecho(self.label+" * "+other.label, np.concatenate([self.valores, other.valores]), self.taxa)

    def __add__(self, other):
        if(self.taxa!=other.taxa):
            print("ERRO: sinais com taxas diferentes")
            return None
        else:
            return Trecho(self.label+" + "+other.label, self.valores+other.valores, self.taxa)

    def __sub__(self, other):
        if(self.taxa!=other.taxa):
            print("ERRO: sinais com taxas diferentes")
            return None
        else:
            return Trecho(self.label+" - "+other.label, self.valores-other.valores, self.taxa)
    
    def __getitem__(self, val):
        if type(val)==slice:
            start = round(val.start*self.taxa) if val.start else 0
            stop = round(val.stop*self.taxa) if val.stop else len(self.valores)
            temp = np.copy(self.valores[start:stop])
            return Trecho(self.label+"[{}:{}]".format(start,stop), temp, self.taxa)
        else:
            return self.valores[round(val*self.taxa)]
    
    def __len__(self):
        return (len(self.valores)/self.taxa)

    def __lt__(self, other):
        return (self.media < other.media)
    
    def __le__(self, other):
        return (self.media <= other.media)

    def __eq__(self, other):
        return (self.media == other.media)

    def __ne__(self, other):
        return (self.media != other.media)
    
    def __ge__(self, other):
        return (self.media >= other.media)

    def __gt__(self, other):
        return (self.media > other.media)

if __name__ == "__main__":
    vals1 = np.random.normal(-1,1,size=50)
    vals2 = np.random.normal(-1,1,size=50)
    t1 = Trecho("Sinal1", vals1, 4)
    t2 = Trecho("Sinal1", vals2, 4)
    print(t1[2:7])
    print(t1+t2)
    print(t1*t2)
    print(t1-t1)
    print(t1>t2)